# Instalar My Robot Lab con Perception Neuron
1. Instalar Eclipse Mars
2. Instalar Axis Neuron v3.5.24
3. Agregar libreria NeuronDataReader b12 a la carpeta System32
4. Clonar repositorios de bitbucket
5. Importar proyecto repo en Eclipse
6. Importar proyecto my-robot-lab en eclipse
7. Configurar Run de eclipse.
8. Ejecutar python Script en MRL

## 1. Instalar Eclipse Mars
Descomprimir la carpeta `eclipse-jee-mars-2-win32-x86_64.zip` dentro
de `C:\Archivos de Programa` o `C:\Program Files` luego crear un acceso directo
al ejecutable `eclipse.exe`


## 2. Instalar Axis Neuron v3.5.24
El instalador de Axis Neuron se encuentra en la carpeta
`Axis_Neuron_x64_3_5_24_2740_20151019094107760000`
se debe configurar para que haga broadcast del BVH en el puerto `7001`
con Format: Binary, Rotation: YXZ, Displacement desactivado, Reference desactivado.

**NOTA:** Esta version de Axis Neuron tiene problema que usa mucho CPU aunque el
si se utiliza una version mas reciente Java deja de funcionar al conectarse con
el API b12

## 3. Agregar libreria NeuroDataReader a la carpeta System32
Dentro de la carpeta _NeuronDataReader b12_ se encuentran dos archivos(`NeuronDataReader.dll`,
`NeuronDataReader.lib`) estos archivos deben copiarse dentro de la carpeta
`C:\Windows\System32`, La libreria es la _version b12_

## 4. Clonar repositorios de bitbucket
Crear una carpeta donde se clonaran los repositorios, luego de haber creado la
carpeta abrir una terminal/bash y ubicarse donde crearon la carpeta para clonar
ambos repositorios.

Clonar el repositorio de dependencias (puede tardar bastante por que son como 800Mb):
```
git clone https://diegocdl@bitbucket.org/turing-lab/repo.git
```

Clonar el repositorio de MyRobotLab:
```
git clone https://diegocdl@bitbucket.org/turing-lab/my-robot-lab.git
```

## 5. Importar proyecto repo en Eclipse
Abrir eclipse e ir a File->import

![import proyect.PNG](https://bitbucket.org/repo/ajzEad/images/2524448377-import%20proyect.PNG)
Seleccionar Existing Projects into Workspace
y dar click en `Next >` luego saldra la siguiente ventana, en el campo de Select root directory: deben dar click a Browse y buscar el repositorio `repo` que se clono en el paso anterior. 

![import project 2.PNG](https://bitbucket.org/repo/ajzEad/images/2310685955-import%20project%202.PNG)

verificar que dentro de la caja de Projects: se encuentre repo marcado luego dar click en `Finish`

## 6. Importar proyecto my-robot-lab en eclipse
Repetir los pasos de la sección anterior pero en lugar de buscar el repositorio `repo` se debe buscar el repositorio `my-robot-lab`

## 7. Configurar Run de eclipse.
Buscar el archivo `Runtime` en `src->org.myrobotlab.service` dar click derecho sobre el archivo para abrir el menú contextual y dar click en `Run As -> 2 Java Application`

![run as.png](https://bitbucket.org/repo/ajzEad/images/2781124881-run%20as.png)

luego ir a `Run->Run configurations...` y en la pestaña de Arguments colocar:

```
-service gui GUIService python Python -logLevel ERROR
```

dentro del cuadro de Program arguments:

![program arguments.PNG](https://bitbucket.org/repo/ajzEad/images/754782842-program%20arguments.PNG)

## 8. Ejecutar python Script en MRL
Script para perception con brazo izquierdo nada mas
```python
leftPort = "COM6" # se debe cambiar por el puerto donde este el arduino 
i01 = Runtime.createAndStart("i01", "InMoov")
i01.startLeftArm(leftPort)
i01.startLeftHand(leftPort)
i01.startPerceptionTracking()
```